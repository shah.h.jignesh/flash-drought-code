import warnings
warnings.filterwarnings("ignore")
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from matplotlib import cm
from mpl_toolkits.basemap import Basemap
import netCDF4 as nc


path = "C:/Users/Admin/Desktop/Imp/Internship/"
SM_File = "Pentad_SMI_1950_2019.nc"
SM_Data = nc.Dataset(path + SM_File)
SM = SM_Data["SM"][:,:,:]
lon = SM_Data["lon"][:]
lat = SM_Data["lat"][:]
ntim = np.shape(SM)[0]
nlat = np.shape(SM)[1]
nlon = np.shape(SM)[2]

#Europe Mask
Europe_SM_Code = np.zeros((nlat,nlon))

for i in range(nlon):
    for j in range(nlat):
        if SM.data[0,j, i] > 0:
            Europe_SM_Code[j,i] = 1
            else:
            Europe_SM_Code[j,i] = 0

del SM

Anomalies_File = "Temperature_Precipitation_Anomaly_During_Events_1950_2019_Remove_First_Last.nc"
Anomalies_Data = nc.Dataset(path + Anomalies_File)
Temperature_Anomalies = Anomalies_Data.variables["Temperature"][:,:,:]
Precipitation_Anomalies = Anomalies_Data.variables["Precipitation"][:,:,:]
Temperature_Anomalies_Pre_1985 = Temperature_Anomalies[0:2555,:,:]
Temperature_Anomalies_Post_1985 = Temperature_Anomalies[2555:5110,:,:]
Precipitation_Anomalies_Pre_1985 = Precipitation_Anomalies[0:2555,:,:]
Precipitation_Anomalies_Post_1985 = Precipitation_Anomalies[2555:5110,:,:]
Precipitation_Anomalies_Pre_1985.data[Precipitation_Anomalies_Pre_1985.data == -9999.0] = "nan"
Precipitation_Anomalies_Post_1985.data[Precipitation_Anomalies_Post_1985.data == -9999.0] = "nan"
Temperature_Anomalies_Pre_1985.data[Temperature_Anomalies_Pre_1985.data == -9999.0] = "nan"
Temperature_Anomalies_Post_1985.data[Temperature_Anomalies_Post_1985.data == -9999.0] = "nan"

Pentads_Number = np.zeros((ntim, nlat, nlon))
for i in range(nlon):
    for j in range(nlat):
        if Europe_SM_Code[j,i] == 1:
            for p in range(0, 5110, 73):
                for q in range(73, 5183, 73):
                    if q - p == 73:
                        Pentads_Number[range(p, q, 1), j, i] = range(1, 74, 1)


Events_File = "Pentad_Flash_Drought_Starting_Data_1950_2019.nc"
Events_Data = nc.Dataset(path + Events_File)
Events_Start = Events_Data.variables["Start"][:,:,:]

Temperature_Data_GS = np.zeros((ntim, nlat, nlon))
Precipitation_Data_GS = np.zeros((ntim, nlat, nlon))

for i in range(nlon):
    for j in range(nlat):
        if Europe_SM_Code[j,i] == 1:
            for t in range(ntim):
                if Pentads_Number[t, j, i] > 18 and Pentads_Number[t,j,i] < 56 and pd.notnull(Temperature_Anomalies.data[t,j,i]) == True:
                    Temperature_Data_GS[t,j,i] = Temperature_Anomalies.data[t,j,i]
                    Precipitation_Data_GS[t,j,i] = Precipitation_Anomalies.data[t,j,i]

Temp_Grow_Pre_1985 = Temperature_Data_GS[0:2555, :,:]
Prep_Grow_Pre_1985 = Precipitation_Data_GS[0:2555,:,:]
Temp_Grow_Pre_1985[Temp_Grow_Pre_1985 == -9999.0] = "nan"
Prep_Grow_Pre_1985[Prep_Grow_Pre_1985 == -9999.0] = "nan"
Temp_Grow_Post_1985 = Temperature_Data_GS[2555:5110,:,:]
Prep_Grow_Post_1985 = Precipitation_Data_GS[2555:5110,:,:]
Temp_Grow_Post_1985[Temp_Grow_Post_1985 == -9999.0] = "nan"
Prep_Grow_Post_1985[Prep_Grow_Post_1985 == -9999.0] = "nan"

First_Quadrant_GS = np.zeros((2, nlat, nlon))
for i in range(nlon):
    for j in range(nlat):
        count = 0
        count_post = 0
        if Europe_SM_Code[j,i] == 1:
            for t in range(2555):
                if Temp_Grow_Pre_1985[t,j,i] > 0 and Prep_Grow_Pre_1985[t,j,i] < 0:
                    count += 1

                if Temp_Grow_Post_1985[t, j, i] > 0 and Prep_Grow_Post_1985[t, j, i] < 0:
                        count_post += 1

        First_Quadrant_GS[0, j, i] = (count)
        First_Quadrant_GS[1, j, i] = (count_post)

First_Quadrant_GS[First_Quadrant_GS == 0] = "nan"


Fourth_Quadrant_GS = np.zeros((2, nlat, nlon))
for i in range(nlon):
    for j in range(nlat):
        count = 0
        count_post = 0
        if Europe_SM_Code[j, i] == 1:
            print([i, "#", j])

            for t in range(2555):
                if Temp_Grow_Pre_1985[t, j, i] < 0 and Prep_Grow_Pre_1985[t, j, i] < 0:
                    count += 1

                if Temp_Grow_Post_1985[t, j, i] < 0 and Prep_Grow_Post_1985[t, j, i] < 0:
                    count_post += 1
        Fourth_Quadrant_GS[0, j, i] = (count)
        Fourth_Quadrant_GS[1, j, i] = (count_post)

Fourth_Quadrant_GS[Fourth_Quadrant_GS == 0] = "nan"

Change_First_Quad_GS = (First_Quadrant_GS[1,:,:] - First_Quadrant_GS[0,:,:])/(First_Quadrant_GS[0,:,:])*100
Change_Second_Quad_GS = (Second_Quadrant_GS[1,:,:] - Second_Quadrant_GS[0,:,:])/(Second_Quadrant_GS[0,:,:])*100
Change_Third_Quad_GS = (Third_Quadrant_GS[1,:,:] - Third_Quadrant_GS[0,:,:])/(Third_Quadrant_GS[0,:,:])*100
Change_Fourth_Quad_GS = (Fourth_Quadrant_GS[1,:,:] - Fourth_Quadrant_GS[0,:,:])/(Fourth_Quadrant_GS[0,:,:])*100

Change_First_Quad_GS[Change_First_Quad_GS == 0] = "nan"

P_Q1 = len(Change_First_Quad_GS[Change_First_Quad_GS > 0])/len(Change_First_Quad_GS[pd.notnull(Change_First_Quad_GS)])
P_Q1 = round(P_Q1*100)
N_Q1 = len(Change_First_Quad_GS[Change_First_Quad_GS < 0])/len(Change_First_Quad_GS[pd.notnull(Change_First_Quad_GS)])
N_Q1 = round(N_Q1*100)


fig, (ax1) = plt.subplots(1,1,figsize = (20,20), dpi = 100)
m = Basemap(projection = "lcc", lon_0 = 10, lat_0 = 52, llcrnrlat = 33, urcrnrlat = 65, llcrnrlon = -10, urcrnrlon = 65, resolution = "c", ax = ax1)
m.drawparallels(np.arange(-20,180,10), labels = [1,0,0,0], fontsize = 45)
m.drawmeridians(np.arange(-10, 70,10), labels = [1,0,0,1], fontsize = 45)
m.drawcoastlines(linewidth = 1.5, linestyle = "solid", color = "k", zorder = 5)
m.shadedrelief(scale = 0.2, alpha = 0.5)
m.fillcontinents(color = "lightgray", zorder = 1)
flon, flat = np.meshgrid(lon, lat)
x,y = m(flon, flat)
Limits = np.arange(7, 42, 7)
pos = ax1.contourf(x, y, First_Quadrant_GS[0,:,:], Limits, cmap = cm.OrRd, extend = "both", zorder = 4)
pos_col = fig.colorbar(pos, orientation = "vertical", ax = ax1, shrink = 0.72)
pos_col.ax.tick_params(labelsize = 60)
fig.tight_layout()
plt.show()

fig, (ax1) = plt.subplots(1,1,figsize = (20,20), dpi = 100)
m = Basemap(projection = "lcc", lon_0 = 10, lat_0 = 52, llcrnrlat = 33, urcrnrlat = 65, llcrnrlon = -10, urcrnrlon = 65, resolution = "c", ax = ax1)
m.drawparallels(np.arange(-20,180,10), labels = [1,0,0,0], fontsize = 45)
m.drawmeridians(np.arange(-10, 70,10), labels = [1,0,0,1], fontsize = 45)
m.drawcoastlines(linewidth = 1.5, linestyle = "solid", color = "k", zorder = 5)
m.shadedrelief(scale = 0.2, alpha = 0.5)
m.fillcontinents(color = "lightgray", zorder = 1)
flon, flat = np.meshgrid(lon, lat)
x,y = m(flon, flat)
Limits = np.arange(7, 42, 7)
pos = ax1.contourf(x, y, First_Quadrant_GS[1,:,:], Limits, cmap = cm.OrRd, extend = "both", zorder = 4)
pos_col = fig.colorbar(pos, orientation = "vertical", ax = ax1, shrink = 0.72)
pos_col.ax.tick_params(labelsize = 60)
fig.tight_layout()
plt.show()


fig, (ax1) = plt.subplots(1,1,figsize = (20,20), dpi = 100)
m = Basemap(projection = "lcc", lon_0 = 10, lat_0 = 52, llcrnrlat = 33, urcrnrlat = 65, llcrnrlon = -10, urcrnrlon = 65, resolution = "c", ax = ax1)
m.drawparallels(np.arange(-20,180,10), labels = [1,0,0,0], fontsize = 45)
m.drawmeridians(np.arange(-10, 70,10), labels = [1,0,0,1], fontsize = 45)
m.drawcoastlines(linewidth = 1.5, linestyle = "solid", color = "k", zorder = 5)
m.shadedrelief(scale = 0.2, alpha = 0.5)
m.fillcontinents(color = "lightgray", zorder = 1)
flon, flat = np.meshgrid(lon, lat)
x,y = m(flon, flat)
Limits = np.arange(-80, 100, 20)
pos = ax1.contourf(x, y, Change_First_Quad_GS, Limits, cmap = cm.BrBG_r, extend = "both", zorder = 4)
x1, y1 = m(-26.7,62.5)
x2, y2 = m(-32, 65)
ax1.text(x2, y2, "+ Change = {}%".format(P_Q1), fontsize = 52, color = "sienna", fontweight = "bold")
ax1.text(x1, y1,"- Change = {}%".format(N_Q1), fontsize = 52, color = "darkcyan", fontweight = "bold")
pos_col = fig.colorbar(pos, orientation = "vertical", ax = ax1, shrink = 0.72)
pos_col.ax.tick_params(labelsize = 60)
fig.tight_layout()
plt.show()


fig, (ax1) = plt.subplots(1,1,figsize = (20,20), dpi = 100)
m = Basemap(projection = "lcc", lon_0 = 10, lat_0 = 52, llcrnrlat = 33, urcrnrlat = 65, llcrnrlon = -10, urcrnrlon = 65, resolution = "c", ax = ax1)
m.drawparallels(np.arange(-20,180,10), labels = [1,0,0,0], fontsize = 45)
m.drawmeridians(np.arange(-10, 70,10), labels = [1,0,0,1], fontsize = 45)
m.drawcoastlines(linewidth = 1.5, linestyle = "solid", color = "k", zorder = 5)
m.shadedrelief(scale = 0.2, alpha = 0.5)
m.fillcontinents(color = "lightgray", zorder = 1)
flon, flat = np.meshgrid(lon, lat)
x,y = m(flon, flat)
Limits = np.arange(2,12, 2)
pos = ax1.contourf(x, y, Fourth_Quadrant_GS[0,:,:], Limits, cmap = cm.OrRd, extend = "both", zorder = 4)
pos_col = fig.colorbar(pos, orientation = "vertical", ax = ax1, shrink = 0.72)
pos_col.ax.tick_params(labelsize = 60)
fig.tight_layout()
plt.show()

fig, (ax1) = plt.subplots(1,1,figsize = (20,20), dpi = 100)
m = Basemap(projection = "lcc", lon_0 = 10, lat_0 = 52, llcrnrlat = 33, urcrnrlat = 65, llcrnrlon = -10, urcrnrlon = 65, resolution = "c", ax = ax1)
m.drawparallels(np.arange(-20,180,10), labels = [1,0,0,0], fontsize = 45)
m.drawmeridians(np.arange(-10, 70,10), labels = [1,0,0,1], fontsize = 45)
m.drawcoastlines(linewidth = 1.5, linestyle = "solid", color = "k", zorder = 5)
m.shadedrelief(scale = 0.2, alpha = 0.5)
m.fillcontinents(color = "lightgray", zorder = 1)
flon, flat = np.meshgrid(lon, lat)
x,y = m(flon, flat)
Limits = np.arange(2, 12, 2)
pos = ax1.contourf(x, y, Fourth_Quadrant_GS[1,:,:], Limits, cmap = cm.OrRd, extend = "both", zorder = 4)
pos_col = fig.colorbar(pos, orientation = "vertical", ax = ax1, shrink = 0.72)
pos_col.ax.tick_params(labelsize = 60)
fig.tight_layout()
plt.show()


Change_Fourth_Quad_GS[Change_Fourth_Quad_GS == 0] = "nan"

P_Q4 = len(Change_Fourth_Quad_GS[Change_Fourth_Quad_GS > 0])/len(Change_Fourth_Quad_GS[pd.notnull(Change_Fourth_Quad_GS)])
P_Q4 = round(P_Q4*100)
N_Q4 = len(Change_Fourth_Quad_GS[Change_Fourth_Quad_GS < 0])/len(Change_Fourth_Quad_GS[pd.notnull(Change_Fourth_Quad_GS)])
N_Q4 = round(N_Q4*100)


fig, (ax1) = plt.subplots(1,1,figsize = (20,20), dpi = 100)
m = Basemap(projection = "lcc", lon_0 = 10, lat_0 = 52, llcrnrlat = 33, urcrnrlat = 65, llcrnrlon = -10, urcrnrlon = 65, resolution = "c", ax = ax1)
m.drawparallels(np.arange(-20,180,10), labels = [1,0,0,0], fontsize = 45)
m.drawmeridians(np.arange(-10, 70,10), labels = [1,0,0,1], fontsize = 45)
m.drawcoastlines(linewidth = 1.5, linestyle = "solid", color = "k", zorder = 5)
m.shadedrelief(scale = 0.2, alpha = 0.5)
m.fillcontinents(color = "lightgray", zorder = 1)
flon, flat = np.meshgrid(lon, lat)
x,y = m(flon, flat)
Limits = np.arange(-80, 100, 20)
pos = ax1.contourf(x, y, Change_Fourth_Quad_GS, Limits, cmap = cm.BrBG_r, extend = "both", zorder = 4)
pos_col = fig.colorbar(pos, orientation = "vertical", ax = ax1, shrink = 0.72)
pos_col.ax.tick_params(labelsize = 60)
x1, y1 = m(-26.7,62.5)
x2, y2 = m(-32, 65)
ax1.text(x2, y2, "+ Change = {}%".format(P_Q4), fontsize = 52, color = "sienna", fontweight = "bold")
ax1.text(x1, y1,"- Change = {}%".format(N_Q4), fontsize = 52, color = "darkcyan", fontweight = "bold")
fig.tight_layout()
plt.show()
