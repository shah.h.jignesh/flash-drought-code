#Packages
import warnings
warnings.filterwarnings("ignore")
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from matplotlib import cm
from mpl_toolkits.basemap import Basemap
import netCDF4 as nc

path = "C:/Users/Admin/Desktop/Imp/Internship/"
SM_File = "Pentad_SMI_1950_2019.nc"
SM_Data = nc.Dataset(path + SM_File)
SM = SM_Data["SM"][:,:,:]
lon = SM_Data["lon"][:]
lat = SM_Data["lat"][:]
ntim = np.shape(SM)[0]
nlat = np.shape(SM)[1]
nlon = np.shape(SM)[2]


#Europe Mask
Europe_SM_Code = np.zeros((nlat,nlon))

for i in range(nlon):
    for j in range(nlat):
        if SM.data[0,j, i] > 0:
            Europe_SM_Code[j,i] = 1
            else:
            Europe_SM_Code[j,i] = 0

del SM

Events_File = "Pentad_Flash_Drought_Starting_Data_1950_2019.nc"
Events_Data = nc.Dataset(path + Events_File)
Events_Start = Events_Data.variables["Start"][:,:,:]

Total_70_Years = np.zeros((2, nlat, nlon))

for i in range(nlon):
    for j in range(nlat):
        if Europe_SM_Code[j,i] == 1:
            Total_70_Years[0,j,i] = np.sum(Yearly_Flash_Drought[0:35,j,i])
            Total_70_Years[1,j,i] = np.sum(Yearly_Flash_Drought[35:70,j,i])

Total_Events_Pre_1985 = Total_70_Years[0,:,:]
Total_Events_Post_1985 = Total_70_Years[1,:,:]


#Yearly Plots
#Pre 1985
fig, (ax1) = plt.subplots(1,1,figsize = (20,20), dpi = 100)

m = Basemap(projection = "lcc", lon_0 = 10, lat_0 = 52, llcrnrlat = 33, urcrnrlat = 65, llcrnrlon = -10, urcrnrlon = 65, resolution = "c", ax = ax1)
m.drawparallels(np.arange(-20,180,10), labels = [1,0,0,0], fontsize = 45)
m.drawmeridians(np.arange(-10, 70,10), labels = [1,0,0,1], fontsize = 45)
m.drawcoastlines(linewidth = 1.5, linestyle = "solid", color = "k", zorder = 5)
m.shadedrelief(scale = 0.2, alpha = 0.5)
m.fillcontinents(color = "lightgray", zorder = 1)
flon, flat = np.meshgrid(lon, lat)
x,y = m(flon, flat)
Limits = np.arange(7,77,7)
pos = ax1.contourf(x, y, Total_Events_Pre_1985, Limits,
cmap = cm.OrRd, extend = "both", zorder = 4)
pos_col = fig.colorbar(pos, orientation = "vertical", ax = ax1, shrink = 0.72)
pos_col.ax.tick_params(labelsize = 60)
fig.tight_layout()
plt.show()


#Post 1985
fig, (ax1) = plt.subplots(1,1,figsize = (20,20), dpi = 100)
m = Basemap(projection = "lcc", lon_0 = 10, lat_0 = 52, llcrnrlat = 33, urcrnrlat = 65, llcrnrlon = -10, urcrnrlon = 65, resolution = "c", ax = ax1)
m.drawparallels(np.arange(-20,180,10), labels = [1,0,0,0], fontsize = 45)
m.drawmeridians(np.arange(-10, 70,10), labels = [1,0,0,1], fontsize = 45)
m.drawcoastlines(linewidth = 1.5, linestyle = "solid", color = "k", zorder = 5)
m.shadedrelief(scale = 0.2, alpha = 0.5)
m.fillcontinents(color = "lightgray", zorder = 1)
flon, flat = np.meshgrid(lon, lat)
x,y = m(flon, flat)
Limits = np.arange(7,77,7)
pos = ax1.contourf(x, y, Total_Events_Post_1985, Limits,
cmap = cm.OrRd, extend = "both", zorder = 4)
pos_col = fig.colorbar(pos, orientation = "vertical", ax = ax1, shrink = 0.72)
pos_col.ax.tick_params(labelsize = 60)
fig.tight_layout()
plt.show()

#Percentage change
fig, (ax3) = plt.subplots(1,1,figsize = (20,20), dpi = 100)
Change_Limits = np.arange(-80,100,20)
o = Basemap(projection = "lcc", lon_0 = 10, lat_0 = 52, llcrnrlat = 33,
urcrnrlat = 65, llcrnrlon = -10, urcrnrlon = 65, resolution = "c", ax = ax3)
o.drawparallels(np.arange(-20, 180, 10), labels = [1,0,0,0], fontsize = 45)
o.drawmeridians(np.arange(-10, 70, 10), labels = [1,0,0,1], fontsize = 45)
o.drawcoastlines(linewidth = 1.5, linestyle = "solid", color = "k", zorder = 5)
o.shadedrelief(scale = 0.2, alpha = 0.5)
o.fillcontinents(color = "lightgray", zorder = 1)
flon, flat = np.meshgrid(lon, lat)
x, y = o(flon, flat)
x1, y1 = m(-26.7,62.5)
x2, y2 = o(-32, 65)
negr = ax3.contourf(x,y, Change,Change_Limits, cmap = cm.BrBG_r, extend = "both", zorder = 4)
ax3.text(x2, y2, "+ Change = {}%".format(Positive_Change), fontsize = 52, color = "sienna", fontweight = "bold")
ax3.text(x1, y1,"- Change = {}%".format(Negative_Change), fontsize = 52, color = "darkcyan", fontweight = "bold")
negr_col = fig.colorbar(negr, orientation = "vertical", ax = ax3, shrink = 0.72, ticks = [-80,-60,-40,-20,0,20,40,60,80])
negr_col.ax.tick_params(labelsize = 60)
fig.tight_layout()
plt.show()

#Growing season frequency
Pentads_Number = np.zeros((ntim, nlat, nlon))
for i in range(nlon):
    for j in range(nlat):
        if Europe_SM_Code[j,i] == 1:
            for p in range(0, 5110, 73):
                for q in range(73, 5183, 73):
                    if q - p == 73:
                        Pentads_Number[range(p, q, 1), j, i] = range(1, 74, 1)

Pentads_Number_Half = Pentads_Number[0:2555,:,:]
Events_Start_Pre_1985 = Events_Start[0:2555,:,:]
Events_Start_Post_1985 = Events_Start[2555:5110,:,:]


Growing_Season = np.zeros((2, nlat, nlon))
for i in range(nlon):
    for j in range(nlat):
        if(Europe_SM_Code[j,i] == 1):
            Growing_Season[0, j,i] = len(Events_Start_Pre_1985[:, j, i][(18 < Pentads_Number_Half[:, j, i]) & (Pentads_Number_Half[:, j, i] < 56) & (Events_Start_Pre_1985[:,j,i] == 1)])
            Growing_Season[1, j,i] = len(Events_Start_Post_1985[:, j, i][(18 < Pentads_Number_Half[:, j, i]) & (Pentads_Number_Half[:,j, i] < 56) & (Events_Start_Post_1985[:,j,i] == 1)])

Growing_Season_Pre_1985 = Growing_Season[0,:,:]
Growing_Season_Post_1985 = Growing_Season[1,:,:]
Growing_Season_Pre_1985[Growing_Season_Pre_1985 == 0] = "nan"
Growing_Season_Post_1985[Growing_Season_Post_1985 == 0] = "nan"

fig, (ax1) = plt.subplots(1,1,figsize = (20,20), dpi = 100)
m = Basemap(projection = "lcc", lon_0 = 10, lat_0 = 52, llcrnrlat = 33, urcrnrlat = 65, llcrnrlon = -10, urcrnrlon = 65, resolution = "c", ax = ax1)
m.drawparallels(np.arange(-20,180,10), labels = [1,0,0,0], fontsize = 45)
m.drawmeridians(np.arange(-10, 70,10), labels = [1,0,0,1], fontsize = 45)
m.drawcoastlines(linewidth = 1.5, linestyle = "solid", color = "k", zorder = 5)
m.shadedrelief(scale = 0.2, alpha = 0.5)
m.fillcontinents(color = "lightgray", zorder = 1)
flon, flat = np.meshgrid(lon, lat)
x,y = m(flon, flat)
Limits = np.arange(7,42,7)
pos = ax1.contourf(x, y, Growing_Season_Pre_1985, Limits,
cmap = cm.OrRd, extend = "both", zorder = 4)
pp = ax1.contourf(x, y, SREX.data, zorder = 7)
pos_col = fig.colorbar(pos, orientation = "vertical", ax = ax1, shrink = 0.72)
pos_col.ax.tick_params(labelsize = 60)
fig.tight_layout()
plt.show()


fig, (ax1) = plt.subplots(1,1,figsize = (20,20), dpi = 100)
m = Basemap(projection = "lcc", lon_0 = 10, lat_0 = 52, llcrnrlat = 33, urcrnrlat = 65, llcrnrlon = -10, urcrnrlon = 65, resolution = "c", ax = ax1)
m.drawparallels(np.arange(-20,180,10), labels = [1,0,0,0], fontsize = 45)
m.drawmeridians(np.arange(-10, 70,10), labels = [1,0,0,1], fontsize = 45)
m.drawcoastlines(linewidth = 1.5, linestyle = "solid", color = "k", zorder = 5)
m.shadedrelief(scale = 0.2, alpha = 0.5)
m.fillcontinents(color = "lightgray", zorder = 1)
flon, flat = np.meshgrid(lon, lat)
x,y = m(flon, flat)
Limits = np.arange(7,42,7)
pos = ax1.contourf(x, y, Growing_Season_Post_1985, Limits,
cmap = cm.OrRd, extend = "both", zorder = 4)
pos_col = fig.colorbar(pos, orientation = "vertical", ax = ax1, shrink = 0.72)
pos_col.ax.tick_params(labelsize = 60)
fig.tight_layout()
plt.show()

fig, (ax3) = plt.subplots(1,1,figsize = (20,20), dpi = 100)
Change_Limits = np.arange(-80,100,20)
o = Basemap(projection = "lcc", lon_0 = 10, lat_0 = 52, llcrnrlat = 33, urcrnrlat = 65, llcrnrlon = -10, urcrnrlon = 65, resolution = "c", ax = ax3)
o.drawparallels(np.arange(-20, 180, 10), labels = [1,0,0,0], fontsize = 45)
o.drawmeridians(np.arange(-10, 70, 10), labels = [1,0,0,1], fontsize = 45)
o.drawcoastlines(linewidth = 1.5, linestyle = "solid", color = "k", zorder = 5)
o.shadedrelief(scale = 0.2, alpha = 0.5)
o.fillcontinents(color = "lightgray", zorder = 1)
flon, flat = np.meshgrid(lon, lat)
x, y = o(flon, flat)
x1, y1 = m(-26.7,62.5)
x2, y2 = o(-32, 65)
negr = ax3.contourf(x,y, Change_Growing,Change_Limits, cmap = cm.BrBG_r, extend = "both", zorder = 4)
ax3.text(x2, y2, "+ Change = {}%".format(Positive_Change_Growing), fontsize = 52, color = "sienna", fontweight = "bold")
ax3.text(x1, y1,"- Change = {}%".format(Negative_Change_Growing), fontsize = 52, color = "darkcyan", fontweight = "bold")
negr_col = fig.colorbar(negr, orientation = "vertical", ax = ax3, shrink = 0.72, ticks = [-80,-60,-40,-20,0,20,40,60,80])
negr_col.ax.tick_params(labelsize = 60)
fig.tight_layout()