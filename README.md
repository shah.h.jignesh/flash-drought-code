# Flash Drought Code

Steps to run code
1. Run SMICreation.R code to convert soil moisture (SM) to soil moisture index (SMI)
2. Run Rapid_Intesification_And_Termination_Condition.R code to find rapid intensification and termination condition.
3. Run Flash_Drought_Identification.R code. As previous step all indentified all the events irrespective to length thus this step will remove events having less than 6 and greater 20
4. Run Flash_Drought_Characterstics_Anomalies.R code. In this step calculated, marked starting flash drought pentad, frequency of flash drought starting every year which helped to calculate frequency of flash drought. Also claculated temperature and and precipitation anomalies which is further used to during event mean temperature and precipitation anomalies.
5. Run Frequency_Plot.py code. To plot total and growing season frequency
6. Run Classes_Plot.py. To plot classes based on growing season frequency
7. Run Heatmap_Plot.R. To plot heatmaps based on frequency of growing season frequency
8. Run SpatialExtent_Code.R. To plot yearly spatial extent of growing season flash droughts

Note: Only growing season flash droughts plot are given here.  
